import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChangePriceThread extends Thread {

    private Shares[] sharesList;
    private static final int TIMER = 19;

    public ChangePriceThread (Shares[] sharesArray) {
        this.sharesList = sharesArray;
    }

    public Shares[] getSharesList() {
        return sharesList;
    }

    public void setSharesList(Shares[] sharesList) {
        this.sharesList = sharesList;
    }

    @Override
    public void run() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-d, HH:mm:ss");
        for(int i = 0; i < 2; i++) {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (i != TIMER) {
                String date = dateFormat.format(new Date());
                System.out.print("\n" + date);
                System.out.print(" There are changes on the stock exchange: ");
                for (Shares share: sharesList) {
                    String oldPrice = new DecimalFormat("#0.00").format(share.getPrice());
                    String newPrice = new DecimalFormat("#0.00").format(share.changePrice());

                    System.out.print("\nPrice for " + share.getName() + " " + oldPrice + " > " + newPrice);
                }
                System.out.println();
            }
        }

        String date = dateFormat.format(new Date());
        System.out.println("\n" + date + " Trading results: ");
        for (Shares share: sharesList) {
            String finalPrice = new DecimalFormat("#0.00").format(share.getPrice());
            System.out.print("\nFinal price for " + share.getName() + " - " + finalPrice +
                    ", remaining amount - " + share.getAmount());
        }
        System.out.println();
    }
}
