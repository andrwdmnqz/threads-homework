import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Customers {

    private String name;
    private Shares[] targets;
    private Shares[] sharesPurchased;

    public Customers(String name, Shares[] targets) {
        this.name = name;
        this.targets = targets;
        sharesPurchased = new Shares[targets.length];

        for(int i = 0; i < sharesPurchased.length; i++) {
            sharesPurchased[i] = new Shares(targets[i].getName(), 0, 0);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Shares[] getTargets() {
        return targets;
    }

    public void setTargets(Shares[] targets) {
        this.targets = targets;
    }

    public Shares[] getSharesPurchased() {
        return sharesPurchased;
    }

    public void setSharesPurchased(Shares[] sharesPurchased) {
        this.sharesPurchased = sharesPurchased;
    }

    public synchronized void tryToBuy(Shares[] currentList) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-d, HH:mm:ss");
        for(int i = 0; i < targets.length; i++) {
            for(int j = 0; j < currentList.length; j++) {
                if (targets[i].getName() == currentList[j].getName()) {
                    String date = dateFormat.format(new Date());
                    String wishingPrice = new DecimalFormat("#0.00").format(targets[i].getPrice());
                    String currentPrice = new DecimalFormat("#0.00").format(currentList[j].getPrice());
                    if(targets[i].getAmount() <= currentList[j].getAmount() && targets[i].getPrice() >= currentList[j].getPrice()) {
                        System.out.println(date + " " + name + " is buying " + targets[i].getAmount() + " " +
                                currentList[j].getName() + " shares for " + currentPrice + " price");
                        currentList[j].setAmount(currentList[j].getAmount() - targets[i].getAmount());
                        getSharesPurchased()[i].setAmount(getSharesPurchased()[i].getAmount() + targets[i].getAmount());
                    } else if(targets[i].getAmount() > currentList[j].getAmount()) {
                        System.out.println(date + " " + name + " is trying to buy " + targets[i].getAmount() + " " +
                                currentList[j].getName() + " shares, but there isn't enough shares to buy");
                    } else if(targets[i].getPrice() < currentList[j].getPrice()) {
                        System.out.println(date + " " + name + " is trying to buy " + targets[i].getAmount() + " " +
                                currentList[j].getName() + " shares for " + wishingPrice +
                                ", but price " + currentPrice + " is too high");
                    }
                }
            }
        }
    }
}
