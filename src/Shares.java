import java.util.Random;

public class Shares {

    private String name;
    private int amount;
    private double price;

    public Shares(String name, int amount, int price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public synchronized double changePrice() {
        Random random = new Random();
        int upOrDown = random.nextInt(0,2);
        //System.out.println("Randomed int: " + upOrDown);
        if (upOrDown == 0) {
            setPrice(price * 1.03);
        }
        else setPrice(price * 0.97);

        return this.price;
    }
}
