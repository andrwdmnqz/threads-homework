import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ExchangeThread extends Thread {

    private Shares[] sharesList;
    private Customers[] customersList;

    public ExchangeThread(Shares[] sharesList, Customers[] customersList) {
        this.sharesList = sharesList;
        this.customersList = customersList;
    }

    public Shares[] getSharesList() {
        return sharesList;
    }

    public void setSharesList(Shares[] sharesList) {
        this.sharesList = sharesList;
    }

    public Customers[] getCustomersList() {
        return customersList;
    }

    public void setCustomersList(Customers[] customersList) {
        this.customersList = customersList;
    }

    @Override
    public void run() {
        Shares[] boughtShares;
        for(int i = 0; i < 120; i++) {
            System.out.println();

            for(Customers customer: customersList) {
                customer.tryToBuy(sharesList);
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-d, HH:mm:ss");
        String date = dateFormat.format(new Date());
        System.out.println("\n" + date + " Customers results: ");
        for(int i = 0; i < customersList.length; i++) {
            for(int j = 0; j < customersList[i].getSharesPurchased().length; j++) {
                System.out.println("Customer " + customersList[i].getName() + " bought " +
                        customersList[i].getSharesPurchased()[j].getAmount() +
                            " shares of " + customersList[i].getSharesPurchased()[j].getName());
            }
        }
    }
}
