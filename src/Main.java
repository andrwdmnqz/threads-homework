import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalDateTime;

public class Main {
    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-d, HH:mm:ss");

        Shares aapl = new Shares("AAPL", 100, 141);
        Shares coke = new Shares("COKE", 1000, 387);
        Shares ibm = new Shares("IBM", 200, 137);

        Shares[] sharesList  = { aapl, coke, ibm };

        Shares[] aliceTarget = { new Shares("AAPL", 10, 100), new Shares("COKE", 20, 390) };
        Shares[] bobTarget = { new Shares("AAPL", 10, 140), new Shares("IBM", 20, 135) };
        Shares[] charlieTarget = { new Shares("COKE", 300, 370) };

        Customers alice = new Customers("Alice", aliceTarget);
        Customers bob = new Customers("Bob", bobTarget);
        Customers charlie = new Customers("Charlie", charlieTarget);

        Customers[] customersList = { alice, bob, charlie };

        ChangePriceThread changePriceThread = new ChangePriceThread(sharesList);
        ExchangeThread exchangeThread = new ExchangeThread(sharesList, customersList);

        changePriceThread.start();
        exchangeThread.start();


//        for(int i = 0; i < 3; i++) {
//            System.out.println("\nCurrent data about shares: \namount of AAPL is " + aapl.getAmount() + ", price is " + aapl.getPrice() +
//                    "\namount of COKE is " + coke.getAmount() + ", price is " + coke.getPrice() +
//                    "\namount of IBM is " + ibm.getAmount() + ", price is " + ibm.getPrice());
//            try {
//                Thread.sleep(3000);
//                String date = dateFormat.format(new Date());
//                System.out.print("\n" + date);
//                System.out.print(" There are changes on the stock exchange: \nPrice for AAPL " + aapl.getPrice() + " > " + aapl.changePrice() +
//                    "\nPrice for COKE " + coke.getPrice() + " > " + coke.changePrice() +
//                    "\nPrice for IBM " + ibm.getPrice() + " > " + ibm.changePrice() + "\n");
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }
    }
}